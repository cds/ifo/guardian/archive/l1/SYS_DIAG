"""SYS DIAGNOSTIC GUARDIAN TESTS

Tests are classes that inherit from DiagTest.  The name of the class
should be the name of the test (e.g. TEST_NAME). They should define a
single test() method:

class TEST_NAME(DiagTest):
    def test(self):
        if something_is_wrong():
            yield MESSAGE

If this method yields a string (i.e. "MESSAGE" above), that string is
raised as a guardian "notification" and the following channel will be
'True':

<IFO>:GRD-SYS_DIAG_NOTIFICATION

The test notifications will be in the GRD-SYS_DIAG_USERMSG* channels,
of the form:

TEST_NAME: MESSAGE

"""
# import the DiagTest class from the main module
from SYS_DIAG import DiagTest

####################
class PRC_GAIN(DiagTest):
    """Check PRC gain above threshold"""
    def test(self):
        a = ezca['LSC-POP_A_LF_OUTPUT']
        b = ezca['IMC-IM4_TRANS_SUM_OUTPUT']
        i = ezca['GRD-ISC_LOCK_STATE_N']
        c=a/b
        if (c < 37) and (i >= 900):
            yield "PRC gain %.2f below threshold. Tweak PRM/ITM alignment to maximize 'LSC_POP_A_LF_OUTPUT'" % c
####################

####################
class PARAMETRIC_INSTABILITY(DiagTest):
    """Check for PI ring up"""
    PI_CHANS = ['IOP-ASC_X_TR_PIT',
                'IOP-ASC_X_TR_YAW',
                'IOP-ASC_Y_TR_PIT',
                'IOP-ASC_Y_TR_YAW',
                ]
    # X is mode at 1360 in DARM
    # Y is mode at 844 in DARM
    max_threshold = 6e6
    min_threshold = -6e6
    def test(self):
        i = ezca['GRD-ISC_LOCK_STATE_N']
        if (i >=925):
            for c in self.PI_CHANS:
                if ezca[c+'_MAX'] > self.max_threshold or \
                        ezca[c+'_MIN'] < self.min_threshold:
                    yield "%s is ringing up!" % c
####################

####################
class SUS_QUAD_PUM_WD(DiagTest):
    """Checks the QUAD PUM RMS WatchDog"""
    def test(self):
        quads = ['ETMX','ETMY','ITMX','ITMY']
        for j in quads:
            if int(ezca['SUS-'+j+'_BIO_L2_MON']) & 2 != 2:
                yield j + " RMS UL WD has tripped"
            if int(ezca['SUS-'+j+'_BIO_L2_MON']) & 32 != 32:
                yield j + " RMS LL WD has tripped"
            if int(ezca['SUS-'+j+'_BIO_L2_MON']) & 512 != 512:
                yield j + " RMS UR WD has tripped"
            if int(ezca['SUS-'+j+'_BIO_L2_MON']) & 8192 != 8192:
                yield j + " RMS LR WD has tripped"
####################

####################
class SUS_QUAD_ESD_DMON(DiagTest):
    """Checks the SUS QUAD ESD Driver Digital Monitors"""
    nominal = 1000
    quads = ['ITMX']
    def test(self):
        for j in self.quads:
            value = ezca['SUS-'+j+'_L3_ESDDMON_LVP_MON']
            if not (self.nominal-100 <= value <= self.nominal+100):
                yield j + " ESD Driver Low Voltage +Ve PSU Fault"
            value = ezca['SUS-'+j+'_L3_ESDDMON_LVN_MON']
            if not (self.nominal-100 <= value <= self.nominal+100):
                yield j + " ESD Driver Low Voltage -Ve PSU Fault"
            value = ezca['SUS-'+j+'_L3_ESDDMON_HVN_MON']
            if not (self.nominal-100 <= value <= self.nominal+100):
                yield j + " ESD Driver High Voltage +Ve PSU Fault"    
            value = ezca['SUS-'+j+'_L3_ESDDMON_HVN_MON']
            if not (self.nominal-100 <= value <= self.nominal+100):
                yield j + " ESD Driver High Voltage -Ve PSU Fault"    
            value = ezca['SUS-'+j+'_L3_ESDDMON_TM1_MON']
            if not (self.nominal-100 <= value <= self.nominal+100):
                yield j + " ESD Driver Temperature Monitor 1 Fault"
            value = ezca['SUS-'+j+'_L3_ESDDMON_TM2_MON']
            if not (self.nominal-100 <= value <= self.nominal+100):
                yield j + " ESD Driver Temperature Monitor 2 Fault"    
            value = ezca['SUS-'+j+'_L3_ESDDMON_TM3_MON']
            if not (self.nominal-100 <= value <= self.nominal+100):
                yield j + " ESD Driver Temperature Monitor 3 Fault"    
            value = ezca['SUS-'+j+'_L3_ESDDMON_TM4_MON']
            if not (self.nominal-100 <= value <= self.nominal+100):
                yield j + " ESD Driver Temperature Monitor 4 Fault"    
            value = ezca['SUS-'+j+'_L3_ESDDMON_TM5_MON']
            if not (self.nominal-100 <= value <= self.nominal+100):
                yield j + " ESD Driver Temperature Monitor 5 Fault"    
            value = ezca['SUS-'+j+'_L3_ESDDMON_TM6_MON']
            if not (self.nominal-100 <= value <= self.nominal+100):
                yield j + " ESD Driver Temperature Monitor 6 Fault"   
            value = ezca['SUS-'+j+'_L3_ESDDMON_CAS_MON']
            if not (self.nominal-100 <= value <= self.nominal+100):
                yield j + " ESD Driver Case Incursion Monitor Fault"    
            value = ezca['SUS-'+j+'_L3_ESDDMON_MCU_MON']
            if not (self.nominal-100 <= value <= self.nominal+100):
                yield j + " ESD Driver Micro-Controller Unit Fault"                       
#################### 

####################
class SUS_QUAD_ESD_DMON(DiagTest):
    """Checks the new SUS QUAD BIO ESD Driver Digital Monitors"""
    quads = ['ETMX','ETMY']
    def test(self):
        for j in self.quads:
            if int(ezca['SUS-'+j+'_BIO_ESD_MON']) & 1 == 1:
                yield j + " ESD Driver Low Voltage +Ve PSU Fault"
            if int(ezca['SUS-'+j+'_BIO_ESD_MON']) & 2 == 2:
                yield j + " ESD Driver Low Voltage -Ve PSU Fault"
            if int(ezca['SUS-'+j+'_BIO_ESD_MON']) & 4 == 4:
                yield j + " ESD Driver High Voltage +Ve PSU Fault"
            if int(ezca['SUS-'+j+'_BIO_ESD_MON']) & 8 == 8:
                yield j + " ESD Driver High Voltage -Ve PSU Fault"
            if int(ezca['SUS-'+j+'_BIO_ESD_MON']) & 16 == 16:
                yield j + " ESD Driver Temperature Monitor 1 Fault"
            if int(ezca['SUS-'+j+'_BIO_ESD_MON']) & 32 == 32:
                yield j + " ESD Driver Temperature Monitor 2 Fault"
            if int(ezca['SUS-'+j+'_BIO_ESD_MON']) & 64 == 64:
                yield j + " ESD Driver Temperature Monitor 3 Fault"
            if int(ezca['SUS-'+j+'_BIO_ESD_MON']) & 128 == 128:
                yield j + " ESD Driver Temperature Monitor 4 Fault"
            if int(ezca['SUS-'+j+'_BIO_ESD_MON']) & 256 == 256:
                yield j + " ESD Driver Temperature Monitor 5 Fault"
            if int(ezca['SUS-'+j+'_BIO_ESD_MON']) & 512 == 512:
                yield j + " ESD Driver Temperature Monitor 6 Fault"
            if int(ezca['SUS-'+j+'_BIO_ESD_MON']) & 1024 == 1024:
                yield j + " ESD Driver Case Incursion Monitor Fault"
            if int(ezca['SUS-'+j+'_BIO_ESD_MON']) & 2048 == 2048:
                yield j + " ESD Driver Micro-Controller Unit Fault"         
#################### 

####################
class SUS_QUAD_ESD_AMON(DiagTest):
    """Checks that QUAD ESD Drivers with a request to drive are active using ESD Analog Monitors"""
    nominal = 0
    quads = ['ITMX']
    def test(self):
        for j in self.quads:
            RDCvalue = int(ezca['SUS-'+j+'_L3_MASTER_OUT_DCMON'])
            if not (RDCvalue == 0):
                MDCvalue = ezca['SUS-'+j+'_L3_ESDAMON_DC_MON']
                if (self.nominal-10 <= MDCvalue <= self.nominal+10):
                    yield j + " ESD Driver Inactive"                  
####################

####################
class ALS_LASER_STATE(DiagTest):
    """ALS Check Lasers are above threshold"""
    def test(self):
        X = ezca['ALS-X_LASER_GR_LF_OUTPUT']
        Y = ezca['ALS-Y_LASER_GR_LF_OUTPUT']
        if X < 300:
            yield " ALS X-end Laser Fault (Off)"
        if Y < 300:
            yield " ALS Y-end Laser Fault (Off)"
####################

####################
class BECKHOFF_STATE(DiagTest):
    """Check for BECKHOFF System Messages"""
    def test(self):
        C = ezca['SYS-C_BECKHOFF_SYSTEM_MESSAGE']
        X = ezca['SYS-X_BECKHOFF_SYSTEM_MESSAGE']
        Y = ezca['SYS-Y_BECKHOFF_SYSTEM_MESSAGE']
        if C == 1:
            yield " BECKHOFF Corner-station Fault"
        if X == 1:
            yield " BECKHOFF X-end Fault"
        if Y == 1:
            yield " BECKHOFF Y-end Fault"   
####################

