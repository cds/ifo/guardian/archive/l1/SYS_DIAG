"""SYS DIAGNOSTIC GUARDIAN

*************************************
***** DO NOT MODIFY THIS MODULE *****
*************************************

This is the control code for the SYS_DIAG guardian node.  It is meant
to load diagnostic tests defined in a SYS_DIAG_tests.py module, which
for your interferometer is located at:

  $USERAPPS/sys/<ifo>/guardian/SYS_DIAG_tests.py

See that file for info about test definitions.

All tests are run in the RUN_TESTS state.  The state status will be
DONE, and the node status will be OK=True, if no tests yield messages.
If they do, status will be RUN and OK=False.

If you request the INIT state, a list of all tests will be printed to
the node log, after which the system will return to RUN_TESTS. 

For help or questions contact:

Jameson Graef Rollins <jrollins@ligo.caltech.edu>

*************************************
***** DO NOT MODIFY THIS MODULE *****
*************************************
"""
##################################################

import inspect

from guardian import GuardState

##################################################

class DiagTest(object):
    """SYS DIAG test definition"""
    def test(self):
        """main test method"""
        return True

##################################################
# import the test definitions.

import SYS_DIAG_tests

# dictionary of all tests
TESTS = {}

for key, obj in inspect.getmembers(SYS_DIAG_tests):

    # reject everything else that's not a class definition
    if not inspect.isclass(obj):
        continue

    # retrieve all base classes of the object
    bases = inspect.getmro(obj)

    # extract object
    if DiagTest not in bases:
        continue
    # ignore the base object itself (each object is first in it's own
    # mro list)
    if bases[0] is DiagTest:
        continue

    TESTS[key] = obj()

##################################################
# actual state definitions

request = 'RUN_TESTS'
nominal = 'RUN_TESTS'

class INIT(GuardState):
    def main(self):
        log("diagnotic tests:")
        for test, inst in TESTS.iteritems():
            doc = inst.__doc__
            if doc:
                doc = doc.strip().split('\n')[0]
            else:
                doc = ''
            log("  %s: %s" % (test, doc))
        return 'RUN_TESTS'

class RUN_TESTS(GuardState):
    index = 10
    def run(self):
        done = True
        for test, inst in TESTS.iteritems():
            #ret = inst.test()
            #if ret:
            for ret in inst.test():
                notify("%s: %s" % (test, ret))
                done = False
        return done

edges = [
    ('INIT', 'RUN_TESTS'),
    ]
